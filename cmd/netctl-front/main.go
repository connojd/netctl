// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/frontend"
	"gitlab.com/redfield/netctl/pkg/modem"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

const (
	frontendDescription = `netctl frontend binds to a specified network interface
and provides an API to manage that interface`
)

// frontendServer is a convenience interface for use here. It simplifies the
// server startup code that needs to handle various frontend types.
type frontendServer interface {
	Initialize(nb *api.BackendMetaData, timeout time.Duration) error
	Serve() error
	Close() error
}

func newWirelessFrontendServer(iface string, addr string) (frontendServer, error) {
	opts := []frontend.WirelessOption{
		frontend.WithConnectLastNetwork(),
		frontend.WithRememberNetworks(),
	}

	if viper.GetBool("assign-uuid") {
		opts = append(opts, frontend.WithUUID())
	}

	if url := viper.GetString("portal-check-url"); url != "" {
		opts = append(opts, frontend.WithManagerOptions(wireless.WithPortalCheckURL(url)))
	}

	return frontend.NewWirelessFrontend(iface, addr, opts...)
}

func newModemFrontendServer(modem string, addr string) (frontendServer, error) {
	return frontend.NewModemFrontend(modem, addr)
}

var (
	rootCmd = &cobra.Command{
		Use:           "netctl-front",
		Short:         "netctl frontend service",
		Long:          frontendDescription,
		SilenceUsage:  true,
		SilenceErrors: true,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if err := viper.BindPFlags(cmd.Flags()); err != nil {
				return err
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				fs frontendServer
			)

			addr := viper.GetString("frontend-address")

			if viper.GetBool("wireless") {
				ifaces, err := wireless.InterfaceNames()
				if err != nil {
					return errors.Wrap(err, "failed to get wireless interface name")
				}

				if len(ifaces) == 0 {
					return errors.New("no wireless interfaces available")
				}

				if len(ifaces) != 1 {
					return errors.New("found multiple wireless interfaces - please specify with --interface")
				}

				fs, err = newWirelessFrontendServer(ifaces[0], addr)
				if err != nil {
					return err
				}

			} else if viper.GetBool("modem") {
				modems, err := modem.GetModems()
				if err != nil {
					return errors.Wrap(err, "failed to get available modems")
				}

				if len(modems) == 0 {
					return errors.New("no modems available")
				}

				if len(modems) != 1 {
					return errors.New("found multiple modems")
				}

				fs, err = newModemFrontendServer(modems[0], addr)
				if err != nil {
					return err
				}

			} else {
				// Determine interface type
				iface := viper.GetString("interface")

				ifType, err := frontend.InterfaceType(iface)
				if err != nil {
					return errors.Wrap(err, "failed to get interface type")
				}

				if ifType != api.InterfaceType_WIRELESS {
					return errors.Errorf("%v is not a supported interface type", iface)
				}

				fs, err = newWirelessFrontendServer(iface, addr)
				if err != nil {
					return err
				}
			}

			tr, err := api.ParseTransport(viper.GetString("backend-address"))
			if err != nil {
				return errors.Wrap(err, "failed to determine backend transport")
			}

			nb := &api.BackendMetaData{
				TransportInfo: tr.Info,
			}

			ec := make(chan error)

			go func() {
				ec <- fs.Serve()
			}()

			timeout := time.Duration(viper.GetInt("timeout"))
			err = fs.Initialize(nb, timeout*time.Second)
			if err != nil {
				return errors.Wrap(err, "failed frontend initialization")
			}

			defer fs.Close()

			c := make(chan os.Signal, 1)
			signal.Notify(c, syscall.SIGTERM)

			select {
			case err := <-ec:
				return err

			case <-c:
				return nil
			}
		},
	}
)

func init() {
	viper.SetEnvPrefix("NETCTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	rootCmd.Flags().String("frontend-address", "", "Frontend address (tcp://host:port")
	rootCmd.Flags().StringP("backend-address", "B", "", "Backend address (tcp://host:port)")
	rootCmd.Flags().StringP("interface", "i", "", "Network interface to bind frontend to")
	rootCmd.Flags().Bool("wireless", false, "Attempt to bind to the wireless interface")
	rootCmd.Flags().Bool("assign-uuid", true, "Assign a UUID to the frontend")
	rootCmd.Flags().String("portal-check-url", "", "URL used for captive portal checking")
	rootCmd.Flags().Bool("modem", false, "Attempt to bind to the mobile broadband modem")
	rootCmd.Flags().Int("timeout", 10, "Timeout (seconds) for the intialization phase")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println("Error creating frontend: ", err)
		os.Exit(-1)
	}
}

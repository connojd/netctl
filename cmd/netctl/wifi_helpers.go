// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

func newBackendClient(addr string) (*api.BackendClient, error) {
	ti, err := api.ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create transport")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	bc := api.NewBackendClient(cc)
	bc.SetTimeout(10 * time.Second)

	return bc, nil
}

func newFrontendClient(nf *api.FrontendMetaData) (*api.FrontendClient, error) {
	ti := &api.Transport{Info: nf.TransportInfo}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	fc := api.NewFrontendClient(cc)

	return fc, nil
}

func connectWirelessOpen(ssid string, hidden bool, disableAuto bool) {
	conf := &api.WirelessNetworkConfiguration{
		Ssid:          ssid,
		KeyMgmt:       wireless.AuthOpen,
		NoAutoConnect: disableAuto,
	}
	if hidden {
		conf.ScanSsid = 1
	}

	doConnect(conf)
}

func connectWirelessPSK(ssid, psk string, hidden bool, disableAuto bool) {
	conf := &api.WirelessNetworkConfiguration{
		Ssid:          ssid,
		KeyMgmt:       wireless.AuthPSK,
		Psk:           psk,
		NoAutoConnect: disableAuto,
	}
	if hidden {
		conf.ScanSsid = 1
	}

	doConnect(conf)
}

// connectSavedNetwork tries to make a connection using a saved
// network configuration. Returns true if a config was found and
// a connection attempt is made, and false if no config was found.
func connectSavedNetwork(ssid string) bool {
	found, err := nfc.WirelessConnectWithSaved(ssid)
	if err != nil {
		fmt.Printf("Failed to connect: %s\n", err)
	}

	if found && err == nil {
		fmt.Printf("Successfully connected to '%s'.\n", ssid)
	}

	return found
}

func doConnect(conf *api.WirelessNetworkConfiguration) {
	state, err := nfc.WirelessConnect(conf)
	if err != nil {
		fmt.Printf("Failed to connect to '%s': %v\n", conf.Ssid, err)
		return
	}

	// Change the "failed" string depending on key mgmt.
	var failedMsg string

	switch conf.KeyMgmt {
	case wireless.AuthPSK:
		failedMsg = fmt.Sprintf("Failed to connect to %s: incorrect password?", conf.Ssid)
	case wireless.AuthOpen:
		failedMsg = fmt.Sprintf("Failed to connect to %s: unknown error.", conf.Ssid)
	default:
		failedMsg = fmt.Sprintf("Failed to connect to %s: check authentication data.", conf.Ssid)
	}

	switch state {
	case api.WirelessState_CONNECTED:
		fmt.Printf("Successfully connected to '%s'.\n", conf.Ssid)
	case api.WirelessState_FAILED:
		fmt.Println(failedMsg)
	case api.WirelessState_PORTAL:
		fmt.Println("Captive portal detected. Run `netctl wifi status` for more info.")
	}
}

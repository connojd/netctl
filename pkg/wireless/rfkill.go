// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"encoding/binary"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

// See https://elixir.bootlin.com/linux/v5.8/source/include/uapi/linux/rfkill.h
// for Linux userspace API definition.

type rfkillType uint8

const (
	// rfkillTypeAll  rfkillType = iota // RFKILL_TYPE_ALL

	rfkillTypeWlan rfkillType = 1 // RFKILL_TYPE_WLAN

	// The following are not needed at this time.
	//
	// RFKILL_TYPE_BLUETOOTH
	// RFKILL_TYPE_UWB
	// RFKILL_TYPE_WIMAX
	// RFKILL_TYPE_WWAN
	// RFKILL_TYPE_GPS
	// RFKILL_TYPE_FM
	// RFKILL_TYPE_NFC
)

type rfkillOp uint8

const (
	// RFKILL_OP_ADD = 0
	// RFKILL_OP_DEL = 1

	rfkillOpChange rfkillOp = 2 // RFKILL_OP_CHANGE

	// RFKILL_OP_CHANGE_ALL = 3
)

type rfkillEvent struct {
	idx  uint32
	typ  rfkillType
	op   rfkillOp
	soft uint8 /* Must be 0 or 1 */
	hard uint8 /* Must be 0 or 1 - should not be set from here */
}

func (e rfkillEvent) bytes() []byte {
	data := make([]byte, 4)
	binary.LittleEndian.PutUint32(data, e.idx)
	data = append(data, byte(e.typ))
	data = append(data, byte(e.op))
	data = append(data, e.soft)
	data = append(data, e.hard)

	return data
}

func rfkillIndexByName(name string) (uint32, error) {
	// Get all rfkill paths in the sysfs.
	paths, err := filepath.Glob("/sys/class/rfkill/rfkill*")
	if err != nil {
		return 0, err
	}

	// The rfkill* directories contain a symlink to the 'device'
	// directory for the device they manage. For network interfaces,
	// there is a path `net/<interface_name>` within that directory.
	//
	// So, for each path, check if path/device/device/net/<name>
	// exists. If it does, then we can read the rfkill index from
	// path/index.
	var path string

	for _, p := range paths {
		dp := filepath.Join(p, "device/device/net", name)

		if _, err := os.Stat(dp); !os.IsNotExist(err) {
			path = p
			break
		}
	}

	if path == "" {
		return 0, errors.Errorf("no rfkill entry for %s", name)
	}

	data, err := ioutil.ReadFile(filepath.Join(path, "index"))
	if err != nil {
		return 0, err
	}

	index, err := strconv.Atoi(strings.TrimSpace(string(data)))
	if err != nil {
		return 0, err
	}

	return uint32(index), nil
}

func rfkillChangeWireless(name string, block bool) error {
	var soft uint8
	if block {
		soft = 1
	}

	idx, err := rfkillIndexByName(name)
	if err != nil {
		return err
	}

	e := rfkillEvent{
		idx:  idx,
		typ:  rfkillTypeWlan,
		op:   rfkillOpChange,
		soft: soft,
	}

	return ioutil.WriteFile("/dev/rfkill", e.bytes(), 0644)
}

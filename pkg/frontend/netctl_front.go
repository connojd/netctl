// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"sync"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api"
)

// frontend holds the common aspects for wired
// and wireless frontends.
type frontend struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedNetctlFrontServer

	uuid string
	typ  api.InterfaceType

	ti *api.Transport

	// Keep track of backend meta data used
	// for backend client
	bmd *api.BackendMetaData
	bc  *api.BackendClient

	init        *initializer
	initSuccess chan bool

	// ctx and cancel func for frontend are used for goroutines
	// that are created as part of initialization, and need to be
	// killed during teardown.
	ctx    context.Context
	cancel context.CancelFunc

	// gRPC server for the frontend
	server *grpc.Server
}

// initState represents the initialization state.
type initState int32

const (
	initStateActive initState = iota + 1
	initStateFailed
	initStateCompleted
	initStateBackendLost
)

// initializer is a helper for the initialization phase for netctl
type initializer struct {
	mux   sync.Mutex
	state initState

	local  chan error
	remote chan error
}

func (i *initializer) setState(state initState) {
	i.mux.Lock()
	defer i.mux.Unlock()

	i.state = state
}

func (i *initializer) getState() initState {
	i.mux.Lock()
	defer i.mux.Unlock()

	return i.state
}

func createBackendClient(nb *api.BackendMetaData) (*api.BackendClient, error) {
	ti := &api.Transport{Info: nb.TransportInfo}

	if ti.String() == "" {
		return nil, errors.New("transport unspecified")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	bc := api.NewBackendClient(cc)

	// Set a reasonable timeout for RPCs.
	bc.SetTimeout(10 * time.Second)

	return bc, nil
}

func (f *frontend) initialize(nb *api.BackendMetaData, timeout time.Duration) error {
	// If the init state is active or completed, bail out.
	//
	// Note that this means initialization can begin if the state is undefined,
	// failed, or backend lost.
	if s := f.init.getState(); s == initStateCompleted || s == initStateActive {
		return errors.Errorf("cannot begin initialization phase due to current init state (state=%v)", s)
	}

	// Allocate buffered channels for init phase.
	f.init.local = make(chan error, 1)
	f.init.remote = make(chan error, 1)

	// Begin the initialization phase.
	f.init.setState(initStateActive)

	// (1) Create a backend client before starting any other init
	//     handlers.
	bc, err := createBackendClient(nb)
	if err != nil {
		f.init.setState(initStateFailed)

		return errors.Wrap(err, "failed to create backend client")
	}

	// (2) Create an error channel to wait for the init
	//     error handler, and start the handler in another
	//     goroutine.
	ec := make(chan error)
	go func() {
		ec <- f.initializationErrorHandler(timeout)
	}()

	// (3) Try to register with the backend. Send any errors over
	//     init.remote so that the init error handler can catch them.
	go func() {
		nf := &api.FrontendMetaData{
			Uuid:          f.uuid,
			Type:          f.typ,
			TransportInfo: f.ti.Info,
		}

		f.init.remote <- bc.RegisterFrontend(nf)
	}()

	// (4) Wait for the verdict from the init error handler.
	//     Note that init.local will be set from handleRegisterAck,
	//     which is triggered by the backend calling Notify.
	if err := <-ec; err != nil {
		f.init.setState(initStateFailed)
		bc.Close()

		return err
	}

	// (5) Set the frontend's backend client and save the
	//     backend metadata.
	f.bc = bc
	f.bmd = nb

	// (6) Finally, set init state to completed.
	f.init.setState(initStateCompleted)

	return nil
}

// initializationErrorHandler watches the frontend's initializer to determine
// if an error occurs remotely or locally, and ensures that the first error
// encountered is the one returned.
func (f *frontend) initializationErrorHandler(timeout time.Duration) error {
	var (
		wg sync.WaitGroup

		// once is used to make sure that err is only
		// set by the first goroutine that encounters
		// an error.
		once sync.Once
		err  error
	)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	wg.Add(2)

	go func() {
		defer wg.Done()

		var e error

		select {
		case e = <-f.init.local:
			e = errors.Wrap(e, "init failed locally")

		case <-ctx.Done():
			e = ctx.Err()
		}

		if e != nil {
			once.Do(func() {
				err = e
			})
		}
	}()

	go func() {
		defer wg.Done()

		var e error

		select {
		case e = <-f.init.remote:
			e = errors.Wrap(e, "init failed remotely")

		case <-ctx.Done():
			e = ctx.Err()
		}

		if e != nil {
			once.Do(func() {
				err = e
			})
		}
	}()

	wg.Wait()

	return err
}

// handleNotify is called by the Notify RPC.
func (f *frontend) handleNotify(n *api.Notification) error {
	switch n.GetType() {
	case api.Notification_REGISTER_ACK:
		return f.handleRegisterAck(n.GetFrontUuid())

	case api.Notification_BACKEND_LEAVING:
		return f.handleBackendLeaving(n.GetFrontUuid())

	default:
		return errors.Errorf("unable to handle notification of type %v", n.GetType())
	}
}

// handleRegisterAck is used during the initialization phase, and is
// how a backend ACK's the frontend registration.
func (f *frontend) handleRegisterAck(uuid string) error {
	if f.init.getState() != initStateActive {
		return errors.New("initialization is not active")
	}

	var err error

	if uuid != f.uuid {
		err = errors.New("this frontend does not match requested frontend")
	}

	f.init.local <- err

	return err
}

// handleBackendLeaving is called when a backend has notified the frontend
// that it is closing.
func (f *frontend) handleBackendLeaving(uuid string) error {
	if f.init.getState() != initStateCompleted {
		return errors.New("frontend is not registered with any backend")
	}

	if uuid != f.uuid {
		return errors.New("this frontend does not match requested frontend")
	}

	f.init.setState(initStateBackendLost)

	f.bc.Close()

	go f.waitForLostBackend()

	return nil
}

func (f *frontend) teardown() error {
	var e error

	if err := f.unregisterWithBackend(); err != nil {
		e = errors.Wrap(err, "failed to unregister with backend")
	}
	f.bc.Close()

	f.init.setState(0)

	// Stop all goroutines that are known to the frontend, e.g.
	// property monitors or AP scan for wireless. Then, do a graceful
	// stop on the gRPC server.
	//
	// It is necessary that f.cancel is called before the server is
	// stopped, as active RPCs may not return until a monitor has
	// died.
	f.cancel()

	if f.server != nil {
		f.server.GracefulStop()
	}

	return e
}

func (f *frontend) unregisterWithBackend() error {
	if s := f.init.getState(); s != initStateCompleted {
		return errors.Errorf("frontend is not registered with any backend (state=%v)", s)
	}

	nf := &api.FrontendMetaData{
		Uuid:          f.uuid,
		Type:          f.typ,
		TransportInfo: f.ti.Info,
	}

	return f.bc.UnregisterFrontend(nf)
}

func (f *frontend) waitForLostBackend() {
	// It's likely that the backend is just restarting.
	// Start off with an aggressive retry rate, and then
	// ease off after a few failures.
	for i := 0; i < 3; i++ {
		err := f.initialize(f.bmd, 5*time.Second)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		return
	}

	for {
		select {
		case <-f.ctx.Done():
			return
		default:
			err := f.initialize(f.bmd, 5*time.Second)
			if err != nil {
				time.Sleep(10 * time.Second)
				continue
			}

			return
		}
	}
}

func (f *frontend) GetMetaData(ctx context.Context, in *api.GetMetaDataRequest) (*api.GetMetaDataReply, error) {
	meta := &api.FrontendMetaData{
		Uuid:          f.uuid,
		Type:          f.typ,
		TransportInfo: f.ti.Info,
	}

	return &api.GetMetaDataReply{Metadata: meta}, nil
}

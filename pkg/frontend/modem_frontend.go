// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"net"
	"os"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/modem"
)

// ModemFrontend is a netctl frontend that is linked to a modem.
type ModemFrontend struct {
	*frontend

	mm *modem.Manager
}

// NewModemFrontend returns a new netctl frontend for a modem.
func NewModemFrontend(mpath string, addr string) (*ModemFrontend, error) {
	ti, err := api.ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invalid address")
	}

	ctx, cancel := context.WithCancel(context.Background())
	frontend := &frontend{
		typ:         api.InterfaceType_MODEM,
		ti:          ti,
		init:        &initializer{},
		initSuccess: make(chan bool, 1),
		ctx:         ctx,
		cancel:      cancel,
	}

	mm, err := modem.NewManager(mpath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create modem manager")
	}

	if err := mm.Enable(); err != nil {
		return nil, errors.Wrap(err, "failed to enable modem")
	}

	mf := &ModemFrontend{
		frontend: frontend,
		mm:       mm,
	}

	uuid, err := hashInterfaceNameWithUUID("modem")
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate uuid for frontend")
	}
	mf.uuid = uuid

	return mf, nil
}

// Serve starts the wireless frontend service.
func (mf *ModemFrontend) Serve() error {
	lis, err := mf.newListener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	mf.server = grpc.NewServer()

	api.RegisterNetctlFrontServer(mf.server, mf)

	// Trigger postInitialization goroutine.
	go mf.postInitialization()

	return mf.server.Serve(lis)
}

func (mf *ModemFrontend) newListener() (net.Listener, error) {
	switch mf.ti.Network() {
	case "tcp":
		return net.Listen(mf.ti.Network(), mf.ti.String())
	case "unix":
		lis, err := net.Listen(mf.ti.Network(), mf.ti.String())
		if err != nil {
			return lis, err
		}

		// Set permissions of the socket so that is world-writable.
		if err := os.Chmod(mf.ti.String(), 0777); err != nil {
			lis.Close()

			return nil, errors.Wrap(err, "failed to create socket listener")
		}

		return lis, nil
	default:
		return nil, errors.Errorf("unknown network type %s", mf.ti.Network())
	}
}

// Close tears down the ModemFrontend
func (mf *ModemFrontend) Close() error {
	if mf.mm != nil {
		_ = mf.mm.Disable()

		if err := mf.mm.Close(); err != nil {
			return err
		}
	}

	return mf.teardown()
}

// Initialize starts the initialization phase and performs startup actions.
func (mf *ModemFrontend) Initialize(nb *api.BackendMetaData, timeout time.Duration) error {
	err := mf.initialize(nb, timeout)
	if err != nil {
		return err
	}

	mf.initSuccess <- true

	return nil
}

// postInitialization performs any actions that should happen as
// a part of service startup, but need to wait until the frontend
// has initialized with the backend.
func (mf *ModemFrontend) postInitialization() {
	select {
	case <-mf.ctx.Done():
		return
	case <-mf.initSuccess:
		break
	}

	go mf.connectWithSavedBearerConfig()
}

// Notify is used to notify a frontend of certain events such as initialization ACK's, and a backend going down.
func (mf *ModemFrontend) Notify(ctx context.Context, r *api.NotifyRequest) (*api.NotifyReply, error) {
	if err := mf.handleNotify(r.GetNotification()); err != nil {
		return nil, err
	}

	return &api.NotifyReply{}, nil
}

func (mf *ModemFrontend) ModemConnect(ctx context.Context, r *api.ModemConnectRequest) (*api.ModemConnectReply, error) {
	bopts := r.GetBearerCfg()

	if err := mf.doConnect(bopts); err != nil {
		return nil, err
	}

	mf.saveBearerConfig(bopts)

	return &api.ModemConnectReply{}, nil
}

func (mf *ModemFrontend) doConnect(cfg *api.BearerConfiguration) error {
	copts := make([]modem.ConnectOption, 0)

	apn := cfg.GetApn()
	if apn == "" {
		return errors.New("must specify APN for connection")
	}

	user, password := cfg.User, cfg.Password
	if user != "" && password != "" {
		copts = append(copts, modem.WithUserAndPassword(user, password))
	}

	if cfg.OverrideDefaultRoaming {
		copts = append(copts, modem.WithAllowRoaming(cfg.AllowRoaming))
	}

	if err := mf.mm.Connect(apn, copts...); err != nil {
		return err
	}

	return nil
}

func (mf *ModemFrontend) connectWithSavedBearerConfig() {
	nf := api.FrontendMetaData{
		Uuid: mf.uuid,
	}

	confs, err := mf.bc.GetSavedBearerConfs(&nf)
	if err != nil {
		log.Print(errors.Wrap(err, "failed to get saved bearer configurations"))
		return
	}

	// Lazily, just use the first one that works. In practice, there
	// should really only be one.
	for _, conf := range confs {
		if err := mf.doConnect(conf); err != nil {
			log.Printf("Failed to connect with saved configuration for %v", conf.GetApn())
		} else {
			log.Printf("Connected with saved configuration for %v", conf.GetApn())

			// Connection was successful, no need to try any more.
			break
		}
	}
}

func (mf *ModemFrontend) saveBearerConfig(conf *api.BearerConfiguration) {
	f := &api.FrontendMetaData{
		Uuid: mf.uuid,
	}

	err := mf.bc.SaveBearerConf(f, conf)
	if err != nil {
		log.Printf("Failed to save bearer configuration for '%v'", conf.GetApn())
	}
}

func (mf *ModemFrontend) ModemDisconnect(ctx context.Context, r *api.ModemDisconnectRequest) (*api.ModemDisconnectReply, error) {
	if err := mf.mm.Disconnect(); err != nil {
		return nil, err
	}

	return &api.ModemDisconnectReply{}, nil
}

func (mf *ModemFrontend) ModemGetProperties(ctx context.Context, r *api.ModemGetPropertiesRequest) (*api.ModemGetPropertiesReply, error) {
	state, err := mf.mm.State()
	if err != nil {
		return nil, err
	}

	props := &api.ModemProperties{
		State: state.String(),
		Apn:   mf.mm.CurrentAPN(),
	}

	return &api.ModemGetPropertiesReply{Props: props}, nil
}

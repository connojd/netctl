import React, {Component} from 'react';

import NetworkListItem from './network_list_item';
import SavedNetworkOptionsDialog from './saved_network_options_dialog';
import Button from './button';

import {ipcRenderer} from 'electron';

export default class SavedNetworksList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            networks: [],
            selectedItem: null,
        };

        this.itemSelected = this.itemSelected.bind(this);
        this.itemUnselected = this.itemUnselected.bind(this);
        this.onClickForgetAll = this.onClickForgetAll.bind(this);

        // Bind and register event handlers.
        this.handleUpdateSavedNetworksList = this.handleUpdateSavedNetworksList.bind(this);

        ipcRenderer.on('update-saved-networks-list', this.handleUpdateSavedNetworksList);
    }

    handleUpdateSavedNetworksList(event, arg) {
        this.setState((state) => ({ ...state, networks: arg}));
    }

    itemSelected(item) {
        this.setState((state) => ({
            ...state,
            selectedItem: {
                ssid: item.ssid,
                security: item.security,
            },
        }));
    }

    itemUnselected() {
        this.setState((state) => ({ ...state, selectedItem: undefined }));
    }

    onClickForgetAll() {
        ipcRenderer.send('forget-network', {forgetAll: true});
    }

    render() {
        // If the network list is empty, display a message.
        if (this.state.networks.length === 0) {
            return (
                <div className='info-display'>
                    There are no saved WiFi networks.
                </div>
            );
        }

        let savedNetworkItems = [];

        this.state.networks.forEach((net) => {
            let item = (
                <NetworkListItem
                    key={net.ssid}
                    ssid={net.ssid}
                    security={net.security}
                    signal={'excellent'}
                    itemClicked={this.itemSelected}
                />
            );

            if (this.state.selectedItem && net.ssid === this.state.selectedItem.ssid) {
                item = (
                    <SavedNetworkOptionsDialog
                        key={net.ssid}
                        ssid={net.ssid}
                        security={net.security}
                        itemClicked={this.itemUnselected}
                    />
                );
            }

            savedNetworkItems.push(item);
        });

        let component = (
            <div className={'saved-networks-container'}>
                <div className={'saved-networks-list-label'}>Known Networks
                </div>
                <div className={'network-list'}>
                    {savedNetworkItems}
                </div>
                <Button
                    className={'saved-network-forget-all-button'}
                    onClick={this.onClickForgetAll}>
                    Forget All Networks
                </Button>
            </div>
        );

        return component;
    }
}

import React, {Component} from 'react';

import Button from './button';
import InputField from './input_field';

// AuthenticationDialogPSK is the dialog used for WPA/WPA2 networks with basic
// PSK authentication.
export default class AuthenticationDialogPSK extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: undefined,
        };

        this.onChangePasswordField = this.onChangePasswordField.bind(this);
        this.onClickNext = this.onClickNext.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    onChangePasswordField(value) {
        this.setState((state) => ({...state, password: value}));
    }

    onClickNext(event) {
        event.stopPropagation();

        this.props.submitAuthData({password: this.state.password});
    }

    onKeyPress(event) {
        if (event.key !== 'Enter') {
            return;
        }

        event.stopPropagation();

        // If the user hits enter, it's logically the
        // same as clicking 'Next.'
        this.props.submitAuthData({password: this.state.password});
    }

    render() {
        let component = (
            <div className={'network-authentication-dialog'}>
                <InputField
                    onChange={this.onChangePasswordField}
                    onKeyPress={this.onKeyPress}
                    value={this.state.password}
                    type={'password'}
                    label={'Enter the network security key'}
                />
                <div className={'dialog-nav-row'}>
                    <Button
                        onClick={this.onClickNext}
                        className={'dialog-nav-button'}>
                        Next
                    </Button>
                    <Button
                        onClick={this.props.onClickCancel}
                        className={'dialog-nav-button'}>
                        Cancel
                    </Button>
                </div>
            </div>
        );

        return component;
    }
}

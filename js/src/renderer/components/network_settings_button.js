import React, {Component} from 'react';
import {ipcRenderer} from 'electron';
import Button from './button';

export default class NetworkSettingsButton extends Component {
    constructor(props) {
        super(props);

        this.onClickNetworkSettings = this.onClickNetworkSettings.bind(this);
    }

    onClickNetworkSettings(event) {
        event.stopPropagation();
        ipcRenderer.send('network-settings-button-clicked', null);
    }

    render() {

        const component = (
            <Button
                className={'network-settings-button'}
                onClick={this.onClickNetworkSettings}>
                Network Settings
            </Button>
        );

        return component;
    }
}

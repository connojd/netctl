const grpc = require('grpc');
const path = require('path');

const {BrowserWindow,nativeImage,ipcMain,screen} = require('electron');

const netctl = require('../api/netctl');

class NetworkSettingsController {
    constructor(netctlFrontServerAddress, netctlBackServerAddress, indexFile) {
        this.netctlFrontServerAddress = netctlFrontServerAddress;
        this.netctlBackServerAddress = netctlBackServerAddress;
        this.netctlFrontUUID = null;
        this.indexFile = indexFile;
        this.browserWindow = null;
        this.netctlFrontClient = null;
        this.netctlBackClient = null;

        this.bindMethods();
        this.bindRendererEvents();
    }

    bindMethods() {
        this.initNetctlFrontClient = this.initNetctlFrontClient.bind(this);
        this.initNetctlBackClient = this.initNetctlBackClient.bind(this);
        this.initNetworkInterfaceInfo = this.initNetworkInterfaceInfo.bind(this);
        this.showNetworkSettingsMenu = this.showNetworkSettingsMenu.bind(this);
        this.handleForgetNetworkEvent = this.handleForgetNetworkEvent.bind(this);
    }

    bindRendererEvents() {
        ipcMain.on('network-settings-menu-ready',
            () => this.initNetctlFrontClient(this.netctlFrontServerAddress)
        );

        ipcMain.on('network-settings-button-clicked',
            () => this.showNetworkSettingsMenu()
        );

    }

    initNetctlFrontClient(address) {
        let client = new netctl.clients.NetctlFrontClient(
            address,
            grpc.credentials.createInsecure()
        );

        grpc.waitForClientReady(client, Date.now() + (3 * 1000), (err) => {
            if (err) {
                console.log('Failed to initialize netctl client, retrying... (err=' + err + ')');
                setTimeout(() => this.initNetctlFrontClient(address), 2000);
            } else {
                this.netctlFrontClient = client;
                this.initNetworkInterfaceInfo();

                // The netctl back client is initialized here since the netctl
                // app currently is designed to support one frontend. To do
                // anything useful with netctl back, we need the fronted's UUID
                // which we can get directly once we have a frontend client.
                //
                // Eventually, if a "find all frontends" approach is used, the
                // netctl back client will be initialized first, and the metadata
                // will be used to contruct the frontend client(s) in the first place.
                let req = new netctl.types.GetMetaDataRequest();
                this.netctlFrontClient.getMetaData(req, (err, resp) => {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    this.netctlFrontUUID = resp.getMetadata().getUuid();
                    this.initNetctlBackClient(this.netctlBackServerAddress);
                });
            }
        });
    }

    initNetctlBackClient(address) {
        let client = new netctl.clients.NetctlBackClient(
            address,
            grpc.credentials.createInsecure()
        );

        grpc.waitForClientReady(client, Date.now() + (3 * 1000), (err) => {
            if (err) {
                console.log('Failed to initialize netctl-back client, retrying... (err=' + err + ')');
                setTimeout(() => this.initNetctlBackClient(address), 2000);
            } else {
                this.netctlBackClient = client;
                this.initSavedNetworksList()
                ipcMain.on('forget-network', this.handleForgetNetworkEvent);
            }
        });
    }

    initNetworkInterfaceInfo() {
        let req = new netctl.types.WirelessGetPropertiesRequest();

        this.netctlFrontClient.wirelessGetProperties(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            const props = resp.getProps();
            this.updateNetworkInterfaceNotify(props);
        });
    }

    initSavedNetworksList() {
        let req = new netctl.types.GetSavedNetworksRequest();
        let meta = new netctl.types.FrontendMetaData();

        meta.setUuid(this.netctlFrontUUID);
        req.setFrontend(meta);

        this.netctlBackClient.getSavedNetworks(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            let networks = [];
            resp.getNetworksList().forEach((net) => {
                const network = {
                    ssid: net.getSsid(),
                    security: net.getKeyMgmt(),
                };

                networks.push(network);
            });

            this.browserWindow.webContents.send('update-saved-networks-list', networks);
        });
    }

    handleForgetNetworkEvent(event, arg) {
        let meta = new netctl.types.FrontendMetaData();
        meta.setUuid(this.netctlFrontUUID);

        if (arg.forgetAll) {
            let req = new netctl.types.ForgetAllNetworksRequest();
            req.setFrontend(meta);

            this.netctlBackClient.forgetAllNetworks(req, (err, resp) => {
                if (err) {
                    console.log(err);
                    return;
                }

                this.initSavedNetworksList();
            });

            return;
        }

        let req = new netctl.types.ForgetNetworkRequest();
        let cfg = new netctl.types.WirelessNetworkConfiguration();

        cfg.setSsid(arg.ssid);
        req.setNetwork(cfg);
        req.setFrontend(meta);

        this.netctlBackClient.forgetNetwork(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            this.initSavedNetworksList();
        });
    }

    updateNetworkInterfaceNotify(wirelessProperties) {
        const networkInterfaceInfo = {
            interfaceName: wirelessProperties.getIfaceName(),
            interfaceIpAddress: wirelessProperties.getIpAddress()
        };

        this.browserWindow.webContents.send('update-network-interface', networkInterfaceInfo);
    }

    showNetworkSettingsMenu() {
        if (this.browserWindow) {
            this.browserWindow.focus();
            return;
        }

        const windowOptions = {
            title: 'Network Settings',
            width: 600,
            height: 600,
            show: true,
            fullscreenable: false,
            resizable: true,
            webPreferences: {
              backgroundThrottling: false,
              nodeIntegration: true,
            }
        };

        this.browserWindow = new BrowserWindow(windowOptions);
        this.browserWindow.loadURL(this.indexFile);
        this.browserWindow.setMenu(null);
        this.browserWindow.on('close', () => this.browserWindow = null);
    }
}

module.exports = NetworkSettingsController;

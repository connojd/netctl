// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"context"
	"time"

	"google.golang.org/grpc"
)

// BackendClient provides a client for the netctl_back service.
type BackendClient struct {
	c NetctlBackClient

	// Keep track so it can be closed later.
	cc *grpc.ClientConn

	// Timeout for RPC calls
	timeout time.Duration
}

// NewBackendClient returns a new netctl_back client.
func NewBackendClient(cc *grpc.ClientConn) *BackendClient {
	bc := &BackendClient{
		c:       NewNetctlBackClient(cc),
		cc:      cc,
		timeout: 30 * time.Second,
	}

	return bc
}

// SetTimeout sets the timeout used to call non-streaming RPCs.
func (bc *BackendClient) SetTimeout(timeout time.Duration) {
	bc.timeout = timeout
}

// Close closes the backend client connection.
func (bc *BackendClient) Close() error {
	return bc.cc.Close()
}

// RegisterFrontend registers a frontend with the backend.
func (bc *BackendClient) RegisterFrontend(nf *FrontendMetaData) error {
	r := &RegisterFrontendRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.RegisterFrontend(ctx, r)

	return err
}

// UnregisterFrontend unregisters a frontend with the backend.
func (bc *BackendClient) UnregisterFrontend(nf *FrontendMetaData) error {
	r := &UnregisterFrontendRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.UnregisterFrontend(ctx, r)

	return err
}

// SaveNetwork saves a network configuration on the backend.
func (bc *BackendClient) SaveNetwork(nf *FrontendMetaData, network *WirelessNetworkConfiguration) error {
	r := &SaveNetworkRequest{
		Frontend: nf,
		Network:  network,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.SaveNetwork(ctx, r)

	return err
}

// GetSavedNetworks returns a list of saved networks for a frontend.
func (bc *BackendClient) GetSavedNetworks(nf *FrontendMetaData) ([]*WirelessNetworkConfiguration, error) {
	r := &GetSavedNetworksRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetSavedNetworks(ctx, r)

	return reply.GetNetworks(), err
}

// ForgetNetwork removes a network from the list of saved networks for a given frontend.
func (bc *BackendClient) ForgetNetwork(nf *FrontendMetaData, network *WirelessNetworkConfiguration) error {
	r := &ForgetNetworkRequest{
		Frontend: nf,
		Network:  network,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.ForgetNetwork(ctx, r)

	return err
}

// GetAllFrontends returns a list of registered frontends.
func (bc *BackendClient) GetAllFrontends() ([]*FrontendMetaData, error) {
	r := &GetAllFrontendsRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetAllFrontends(ctx, r)

	return reply.GetFrontends(), err
}

// GetFrontend returns a frontend matching the specified UUID, if any.
func (bc *BackendClient) GetFrontend(uuid string) (*FrontendMetaData, error) {
	r := &GetFrontendRequest{
		Uuid: uuid,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetFrontend(ctx, r)

	return reply.GetFrontend(), err
}

// SaveBearerConf saves a bearer configuration on the backend.
func (bc *BackendClient) SaveBearerConf(nf *FrontendMetaData, conf *BearerConfiguration) error {
	r := &SaveBearerConfRequest{
		Frontend:  nf,
		BearerCfg: conf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.SaveBearerConf(ctx, r)

	return err
}

// GetSavedBearerConfs returns a list of saved bearer configurations for a frontend.
func (bc *BackendClient) GetSavedBearerConfs(nf *FrontendMetaData) ([]*BearerConfiguration, error) {
	r := &GetSavedBearerConfsRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetSavedBearerConfs(ctx, r)

	return reply.GetBearerCfgs(), err
}
